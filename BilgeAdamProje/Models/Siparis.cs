﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilgeAdamProje.Models
{
    public class Siparis:Base       
    {
        public DateTime SiparisTarihi { get; set; }

        public decimal Tutar { get; set; }
    }
}
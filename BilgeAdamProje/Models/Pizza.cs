﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilgeAdamProje.Models
{
    public class Pizza:Base
    {
        public string Adi { get; set; }

        public decimal Fiyat { get; set; }

        public string EkstraMalzeme { get; set; }
    }
}